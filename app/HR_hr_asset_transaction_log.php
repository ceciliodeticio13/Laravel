<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HR_hr_asset_transaction_log extends Model
{
    //
    protected $primaryKey = 'asset_transaction_log_id';
    protected $table = 'hr_asset_transaction_log';
}
